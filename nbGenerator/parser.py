import nbformat as nbf

def load_notebook(name):
	return nbf.read(name,nbf.current_nbformat)

def fix_notebook(nb):
	for cell in nb.cells:
		fix_cell(cell)
	return nb

def save_notebook(nb,name):
	nbf.write(nb,open(name+".ipynb","w"))

def fix_cell(cell):
	if cell['cell_type'] == 'markdown':
		fix_cell_markdown(cell)
	elif cell['cell_type'] == 'code':
		fix_cell_code(cell)
	clear_outputs(cell)

def fix_cell_markdown(cell):
	metadata = cell['metadata']
	if 'purpose' in metadata and metadata['purpose'] == 'solution':
		cell['source'] = 'Enter your solution here'

def fix_cell_code(cell):
	metadata = cell['metadata']
	if 'purpose' in metadata and metadata['purpose'] == 'solution':
		lines = cell['source'].split('\n')
		i = 0
		while i < (len(lines)-1):
			if '=' in lines[i]:
				lines[i] = lines[i].split('=')[0] + '= ... # Write your solution here'
			if 'def ' in lines[i]:
				lines[i+1] = '\t... # Your code here'
				i += 2
				while i < len(lines)-1 and '\t' in lines[i]:
					del lines[i]
			i += 1

		cell['source'] = "\n".join(lines)

def clear_outputs(cell):
	if 'outputs' in cell:
		cell['outputs'] = []
