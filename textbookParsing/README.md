# textbook-tools

This is a set of utilities for expediting creation of the textbook for this class
A plugin in Jupyter allows you to toggle cells to be hidden, and w/ the use of a custom 
template, you can export with the hidden settings

## Usage

1) Copy the js file over to your extensions directory

	cp hiddenToggle.js ~/.ipython/nbextensions/

2) Start Jupyter notebook and enable the extension

	jupyter-notebook
	jupyter nbextension enable hiddenToggle

3) Inside the notebook that you are editing, click on a cell (Command mode), and either press
Alt-H to hide the cell and output, or Alt-C to hide just the cell

4) Export to HTML

	jupyter nbconvert --to html yourfile.ipynb --template=template.tpl

