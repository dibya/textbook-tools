function questionNAME (env) {
				var name = 'NAME';
				var possibleStates = POSSIBLESTATES; 
				currentCell = IPython.notebook.get_selected_cell();
				if(name in currentCell.metadata){
					currentCell.metadata[name] = nextOne(currentCell.metadata[name],possibleStates);
				} else {
					currentCell.metadata[name] = possibleStates[0];
				}
				if(currentCell.element.find("."+name).length  == 0){
					currentCell.element.find(".input_prompt")[0].appendChild(newDivCell(name));
				}
				currentCell.element.find("."+name)[0].innerHTML = currentCell.metadata[name];
}

shortcuts[SHORTCUT] = {help:HELP, handler:questionNAME,actname:'NAME'}