# Metadata Toggle

This is a plugin in Jupyter allows you to toggle cells to be between various states using keyboard shortcuts.  In particular, for stat 140, we use this as a way to denote which cells contain what type of student data for parsing


For the textbook, use the file *default*

For other course materials, use the file *studentNotebooks*


## Installation

1) Look into the *configurations* folder, and select the configuration that you need. You may add your own configuration based on the templates provided inside the folder.

The rest of this guide will assume that you are using the file *configurations/studentNotebooks.json/". Be sure to switch studentNotebooks with whatever file you use.

2) Run make

	make install name="studentNotebooks"

This will search for a file in the form of configurations/<name>.json, so make sure it exists. 


## Usage

Boot up Jupyter Notebook, and open up any notebook. Use the provided keyboard shortcuts for this plugin, while having selected **1** cell in command mode, to toggle between the various states. In particular, we list the commands for our studentNotebooks configuration.

	Alt-h : Toggle between "student" and "solution"
	Alt-c : Toggle between "setup", "OK", and "normal"