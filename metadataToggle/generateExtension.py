import json
import sys

questionText = open('questionTemplate.js').read()
baseText = open('baseTemplate.js').read()

def generateQuestion(arr):
	qt = str(questionText)
	for attr,val in arr.items():
		qt = qt.replace(attr.upper(),val if attr=='name' else repr(val))
	return qt

def generateFile(dictionaryOfArr):
	allTexts = "\n".join([generateQuestion(q) for q in dictionaryOfArr])
	return baseText.replace("QUESTIONS",allTexts)


if __name__ == "__main__":
	if len(sys.argv) == 1:
		fName = input("Please specify a format file:  ")
	else:
		fName = sys.argv[-1]
	dictOfArr = json.load(open(fName))
	f = open("build.js","w")
	f.write(generateFile(dictOfArr))
	f.flush()