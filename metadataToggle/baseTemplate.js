define(['base/js/namespace'], function(IPython){
		function newDivCell(type){
			var hiddenState = document.createElement("div");
			hiddenState.style.color = "#822";
			hiddenState.classList.add(type)
			return hiddenState;
		}
		function nextOne(currentVal,array){
			index = array.indexOf(currentVal);
			return array[(index+1)%array.length];
		}
		shortcuts = {}
	    function _on_load(){
		              console.info('Hello SciPy 2015')
				      window.IPython = IPython;
		//var action_name = IPython.keyboard_manager.actions.register(toggleHidden, 'toggleHidden','scipy-2015')
				          }
		QUESTIONS


		 var cells = IPython.notebook.get_cells();
	    for(var i in cells){
	        var cell = cells[i];
	        for (short in shortcuts){
	        	name = shortcuts[short].actname;
	        	if(name in cell.metadata){
	        		newCell = newDivCell(name)
	        		newCell.innerHTML = cell.metadata[name];
					cell.element.find(".input_prompt")[0].appendChild(newCell);
				}
	        }
	    };
		//var shortcuts = {'Alt-h': {help:'Toggle whether HTML output should hide cell', handler: toggleSolution},
		//	'Alt-c': {help:'Toggle whether code should be hidden in HTML', handler: toggleHidden}};
		IPython.keyboard_manager.command_shortcuts.add_shortcuts(shortcuts);
	        return {load_ipython_extension: _on_load };
})
